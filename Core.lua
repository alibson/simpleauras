-------------------------------------------------------------------------------
-- Create addon
-------------------------------------------------------------------------------

SimpleAuras = LibStub("AceAddon-3.0"):NewAddon("SimpleAuras", "AceConsole-3.0", "AceEvent-3.0")

-------------------------------------------------------------------------------
-- Locals
-------------------------------------------------------------------------------

local L = LibStub("AceLocale-3.0"):GetLocale("SimpleAuras")
local media = LibStub("LibSharedMedia-3.0", true)
local LBF = LibStub('LibButtonFacade', true)

local DebuffTypeColor = _G.DebuffTypeColor
local BUFF_WARNING_TIME = _G.BUFF_WARNING_TIME -- 31s by default, makes buttons flash
local BUFF_DURATION_WARNING_TIME = _G.BUFF_DURATION_WARNING_TIME -- 60s by default, changes duration text color
local HIGHLIGHT_FONT_COLOR = _G.HIGHLIGHT_FONT_COLOR
local NORMAL_FONT_COLOR = _G.NORMAL_FONT_COLOR

local GetPlayerBuff = _G.GetPlayerBuff
local GetPlayerBuffTexture = _G.GetPlayerBuffTexture
local GetPlayerBuffApplications = _G.GetPlayerBuffApplications
local GetPlayerBuffDispelType = _G.GetPlayerBuffDispelType
local GetPlayerBuffTimeLeft = _G.GetPlayerBuffTimeLeft
local GetWeaponEnchantInfo = _G.GetWeaponEnchantInfo
local GetInventoryItemTexture = _G.GetInventoryItemTexture
local ceil = _G.ceil
local CreateFrame = _G.CreateFrame

local alphaValue = 1

local frames = {"BuffFrame", "DebuffFrame", "ImbueFrame"}

-------------------------------------------------------------------------------
-- Format time
-------------------------------------------------------------------------------

local FormatTime

local DAY_ONELETTER_ABBR = _G.DAY_ONELETTER_ABBR
local HOUR_ONELETTER_ABBR = _G.HOUR_ONELETTER_ABBR
local MINUTE_ONELETTER_ABBR = _G.MINUTE_ONELETTER_ABBR
local SECOND_ONELETTER_ABBR = _G.SECOND_ONELETTER_ABBR
local formatFunctions = {
	[1] = function(sec) -- blizzlike
		if sec >= 86400 then
			return DAY_ONELETTER_ABBR, sec / 86400 + 1 -- blizz always rounds up
		elseif sec >= 3600 then
			return HOUR_ONELETTER_ABBR, sec / 3600 + 1
		elseif sec >= 60 then
			return MINUTE_ONELETTER_ABBR, sec / 60 + 1
		else
			return SECOND_ONELETTER_ABBR, sec
		end
	end,
	[2] = function(sec) -- detailed
		if sec < 60 then
			return "%d", sec
		else
			return "%d:%02d", sec / 60, sec % 60
		end
	end,
}

function SimpleAuras:SetTimeFormat()
	FormatTime = formatFunctions[self.db.profile.timeFormat]
end

-------------------------------------------------------------------------------
-- Initialization
-------------------------------------------------------------------------------

function SimpleAuras:OnInitialize()
	self.db = LibStub("AceDB-3.0"):New("SimpleAurasDB", self.defaults, "Default")
	-- config
	LibStub("AceConfig-3.0"):RegisterOptionsTable("SimpleAuras", self:GetOptions()); self.GetOptions = nil
	self:RegisterChatCommand("sa", function() LibStub("AceConfigDialog-3.0"):Open("SimpleAuras") end )
	self:RegisterChatCommand("simpleauras", function() LibStub("AceConfigDialog-3.0"):Open("SimpleAuras") end )
	-- AceDBOptions-3.0
	self.db.RegisterCallback(self, "OnProfileChanged", "RedrawFrames")
	self.db.RegisterCallback(self, "OnProfileCopied", "RedrawFrames")
	self.db.RegisterCallback(self, "OnProfileReset", "RedrawFrames")
	-- LibSharedMedia-3.0
	if media then
		media.RegisterCallback(self, "LibSharedMedia_SetGlobal", "ResetFonts")
		media.RegisterCallback(self, "LibSharedMedia_Registered", "ResetFonts")
	end
	-- ButtonFacade
	if LBF then
		LBF:RegisterSkinCallback("SimpleAuras", self.ButtonFacadeCallback, self)
	end
	-- create frames
	self:InitFrames()
end

function SimpleAuras:OnEnable()
	self:RegisterEvent("PLAYER_AURAS_CHANGED")
	self:RegisterEvent("UNIT_INVENTORY_CHANGED")

	self.BuffFrame:SetScript("OnUpdate", function(this, elapsed) self:FlashFrameOnUpdate(elapsed) end)

	-- apply ButtonFacade skin, groups have to be created after initializing or ButtonFacade config won't work for some reason
	if LBF and not self.unlocked then
		for i = 1, 3 do
			local frameName = frames[i]
			local group = LBF:Group("SimpleAuras", frameName)

			if self.db.profile[frameName].ButtonFacade then
				group:Skin(unpack(self.db.profile[frameName].ButtonFacade))
			elseif frameName == "ImbueFrame" then -- set default imbue border color
				group:Skin(nil, nil, nil, "Border", 0.4, 0.2, 0.6)
			end

			for i = 1, #self[frameName].buttons do
				group:AddButton(self[frameName].buttons[i])
			end
		end
	end
end

-------------------------------------------------------------------------------
-- Event handlers
-------------------------------------------------------------------------------

function SimpleAuras:PLAYER_AURAS_CHANGED()
	-- handle buffs
	local count = 0
	for index = 1, 32 do
		if self:SetButton(index, "HELPFUL") then
			count = count + 1
		end
	end
	self.BuffFrame:SetID(count) -- frame ID stores number of buttons shown

	-- handle debuffs
	count = 0
	for index = 1, 16 do
		if self:SetButton(index, "HARMFUL") then
			count = count + 1
		end
	end
	self.DebuffFrame:SetID(count) -- frame ID stores number of buttons shown
end

function SimpleAuras:UNIT_INVENTORY_CHANGED(event, unit)
	if unit == "player" then
		self:ScanImbues()
	end
end

-------------------------------------------------------------------------------
-- Buff update
-------------------------------------------------------------------------------

function SimpleAuras:SetButton(index, filter)
	local button = filter == "HELPFUL" and self.BuffFrame.buttons[index] or self.DebuffFrame.buttons[index]
	local buffIndex, untilCancelled = GetPlayerBuff(index, filter)

	if buffIndex == 0 then -- no buff
		button:Hide()
		return false
	else -- has buff
		button:SetID(buffIndex)
		button.icon:SetTexture(GetPlayerBuffTexture(buffIndex))
		local count = GetPlayerBuffApplications(buffIndex)
		button.count:SetText(count > 1 and count or "")
		if filter == "HARMFUL" then -- is debuff
			local color = DebuffTypeColor[GetPlayerBuffDispelType(buffIndex) or "none"]
			button.border:SetVertexColor(color.r, color.g, color.b)
		end
		button.untilCancelled = untilCancelled
		self:SetAuraDuration(button, untilCancelled == 0 and GetPlayerBuffTimeLeft(buffIndex) or 0)
		button:Show()
		return true
	end
end

function SimpleAuras:UpdateAuraDurations(frame)
	local button
	for i = 1, frame:GetID() do -- does nothing if ID < 1
		button = frame.buttons[i]
		if button.untilCancelled == 0 then -- don't update text for auras with no duration
			self:SetAuraDuration(button, GetPlayerBuffTimeLeft(button:GetID()))
		end
	end
end

function SimpleAuras:SetAuraDuration(button, duration)
	if duration == 0 then -- no duration
		button.duration:SetText("")
		button.flashing = false
		button:SetAlpha(1)
	else -- has duration
		button.duration:SetFormattedText(FormatTime(duration))
		if duration < BUFF_DURATION_WARNING_TIME then
			button.duration:SetVertexColor(HIGHLIGHT_FONT_COLOR.r, HIGHLIGHT_FONT_COLOR.g, HIGHLIGHT_FONT_COLOR.b)
			if duration < BUFF_WARNING_TIME then -- enable flashing
				button.flashing = true
				return
			end
		else
			button.duration:SetVertexColor(NORMAL_FONT_COLOR.r, NORMAL_FONT_COLOR.g, NORMAL_FONT_COLOR.b)
		end
		button.flashing = false
		button:SetAlpha(1)
	end
end

-------------------------------------------------------------------------------
-- Imbues
-------------------------------------------------------------------------------

function SimpleAuras:ScanImbues()
	local hasMainHandEnchant, mainHandExpiration, mainHandCharges, hasOffHandEnchant, offHandExpiration, offHandCharges = GetWeaponEnchantInfo()

	if hasMainHandEnchant then
		local button = self.ImbueFrame.buttons[1]
		button.icon:SetTexture(GetInventoryItemTexture("player", 16))
		-- button.count:SetText(mainHandCharges > 1 and mainHandCharges or "") -- are there any imbues with charges anymore? blizz frames don't show them
		self:SetAuraDuration(button, mainHandExpiration/1000)
		-- self:SetAuraDuration(button, mainHandExpiration and mainHandExpiration/1000 or nil) -- are there any imbues with nil duration?
		button:Show()
	else
		self.ImbueFrame.buttons[1]:Hide()
	end

	if hasOffHandEnchant then
		local button = self.ImbueFrame.buttons[2]
		button.icon:SetTexture(GetInventoryItemTexture("player", 17))
		-- button.count:SetText(offHandCharges > 1 and offHandCharges or "") -- are there any imbues with charges anymore? blizz frames don't show them
		self:SetAuraDuration(button, offHandExpiration/1000)
		--self:SetAuraDuration(button, offHandExpiration and offHandExpiration/1000 or nil) -- are there any imbues with nil duration?
		button:Show()
	else
		self.ImbueFrame.buttons[2]:Hide()
	end
end

-------------------------------------------------------------------------------
-- Scripts
-------------------------------------------------------------------------------

do
	local flashTime = 0
	local fading = false
	local durationUpdateTimer = 0

	function SimpleAuras:FlashFrameOnUpdate(elapsed)
		-- update alphaValue
		flashTime = flashTime - elapsed
		if flashTime < 0 then
			local overtime = -flashTime
			fading = not fading
			flashTime = 0.75
			if overtime < flashTime then
				flashTime = flashTime - overtime
			end
		end
		if not fading then
			alphaValue = (0.75 - flashTime) / 0.75
		else
			alphaValue = flashTime / 0.75
		end
		alphaValue = (alphaValue * (1 - 0.3)) + 0.3

		-- update durationUpdateTimer
		durationUpdateTimer = durationUpdateTimer + elapsed
		if durationUpdateTimer > 1 then
			self:UpdateAuraDurations(self.BuffFrame)
			self:UpdateAuraDurations(self.DebuffFrame)
			self:ScanImbues()
			durationUpdateTimer = 0
		end
	end
end

function SimpleAuras:FlashButton()
	if self.flashing then
		self:SetAlpha(alphaValue)
	end
end

function SimpleAuras:OnClick()
	CancelPlayerBuff(self:GetID())
end

function SimpleAuras:OnClickImbue(ID)
	if ID == 16 then
		CancelItemTempEnchantment(1)
	elseif ID == 17 then
		CancelItemTempEnchantment(2)
	end
end

function SimpleAuras:ButtonOnLoad()
	self:RegisterForClicks("RightButtonUp")
	local name = self:GetName()
	self.icon = _G[name.."Icon"]
	self.count = _G[name.."Count"]
	self.count:SetJustifyH("RIGHT")
	self.duration = _G[name.."Duration"]
	self.duration:SetPoint("TOP", self, "BOTTOM", 0, -1)
	self.duration:Show()
	if _G[name.."Border"] then
		self.border = _G[name.."Border"]
	end
	self:Hide()
end

function SimpleAuras:FinishedMoving(this)
	this:StopMovingOrSizing()
	local db = self.db.profile[(this:GetName()):sub(5)]
	local point, _, relativePoint, xOffset, yOffset = this:GetPoint()
	db.custom = false -- disable custom anchor
	db.point = point
	db.relativeTo = "UIParent"
	db.relativePoint = relativePoint
	db.xOffset = xOffset
	db.yOffset = yOffset
end

-------------------------------------------------------------------------------
-- Align functions
-------------------------------------------------------------------------------

function SimpleAuras:AlignButtons(frameName)
	local buttons = self[frameName].buttons
	local point, relativeTo, relativePoint, xOffset, yOffset

	local db = self.db.profile[frameName]

	-- first button
	buttons[1]:ClearAllPoints()
	if db.rowDirection == "DOWN" then
		if db.columnDirection == "LEFT" then
			point = "TOPRIGHT";		relativePoint = "TOPRIGHT";		xOffset = -2;	yOffset = -2
		else -- go right
			point = "TOPLEFT";		relativePoint = "TOPLEFT";		xOffset = 2;	yOffset = -2
		end
	else -- go up
		local _, fontSize = buttons[1].duration:GetFont()
		if db.columnDirection == "LEFT" then
			point = "BOTTOMRIGHT";	relativePoint = "BOTTOMRIGHT";	xOffset = -2;	yOffset = 2 + fontSize
		else -- go right
			point = "BOTTOMLEFT";	relativePoint = "BOTTOMLEFT";	xOffset = 2;	yOffset = 2 + fontSize
		end
	end
	buttons[1]:SetPoint(point, self[frameName], relativePoint, xOffset, yOffset)

	local column = 1
	for i = 2, #buttons do
		-- find anchor
		if column >= db.maxColumns then -- start a new row
			relativeTo = buttons[i - db.maxColumns] -- align to first button of previous row
			if db.rowDirection == "DOWN" then
				point = "TOPRIGHT";		relativePoint = "BOTTOMRIGHT";	yOffset = -db.rowOffset
			else -- go up
				point = "BOTTOMRIGHT";	relativePoint = "TOPRIGHT";		yOffset = db.rowOffset
			end
			xOffset = 0
			column = 0 -- reset column counter
		else -- continue previous row
			relativeTo = buttons[i - 1]
			if db.columnDirection == "LEFT" then
				point = "TOPRIGHT";		relativePoint = "TOPLEFT";		xOffset = -db.columnOffset
			else -- go right
				point = "TOPLEFT";		relativePoint = "TOPRIGHT";		xOffset = db.columnOffset
			end
			yOffset = 0
		end
		-- SetPoint
		buttons[i]:ClearAllPoints()
		buttons[i]:SetPoint(point, relativeTo, relativePoint, xOffset, yOffset)
		column = column + 1
	end
end

-------------------------------------------------------------------------------
-- ButtonFacade
-------------------------------------------------------------------------------

function SimpleAuras:ButtonFacadeCallback(SkinID, Gloss, Backdrop, Group, Button, Colors)
	if Group then
		local db = self.db.profile[Group]

		if db.ButtonFacade then
			db.ButtonFacade[1] = SkinID
			db.ButtonFacade[2] = Gloss
			db.ButtonFacade[3] = Backdrop
			db.ButtonFacade[4] = Colors
		else
			db.ButtonFacade = {SkinID, Gloss, Backdrop, Colors}
		end
	end
end

-------------------------------------------------------------------------------
-- LibSharedMedia
-------------------------------------------------------------------------------

function SimpleAuras:ResetFonts(callback, mediatype, key)
	if mediatype == "font" then
		local frameName, redraw
		for i = 1, 3 do
			frameName = frames[i]
			redraw = false
			-- check if redraw needed
			if callback == "LibSharedMedia_Registered" then
				if self.db.profile[frameName].durationFont == key or self.db.profile[frameName].stackFont == key then
					redraw = true
				end
			elseif callback == "LibSharedMedia_SetGlobal" then
				redraw = true
			end
			-- fonts changed, redraw
			if redraw then
				self:StyleButtons(frameName)
			end
		end
	end
end

-------------------------------------------------------------------------------
-- Frames
-------------------------------------------------------------------------------

function SimpleAuras:InitFrames()
	-- hide blizz frames
	_G["BuffFrame"]:UnregisterAllEvents()
	_G["BuffFrame"]:Hide()
	_G["TemporaryEnchantFrame"]:UnregisterAllEvents()
	_G["TemporaryEnchantFrame"]:Hide()

	-- set FormatTime function
	self:SetTimeFormat()

	-- create frame and its buttons
	local frameName, db, frame, buttonName, buttonTemplate
	for i = 1, 3 do
		frameName = frames[i]
		db = self.db.profile[frameName]

		-- create frame
		self[frameName] = CreateFrame("Frame", "SAF_"..frameName, UIParent, "SAFBaseFrameTemplate")
		frame = self[frameName]

		-- create buttons
		frame.buttons = {}
		buttonTemplate = ("SAF_%sTemplate"):format(frameName:sub(0,-6))
		for i = 1, db.maxButtons do
			buttonName = ("SAF_%s_Button%d"):format(frameName, i) -- "SAF_BuffFrame_Button1"
			frame.buttons[i] = CreateFrame("Button", buttonName, frame, buttonTemplate)
			frame.buttons[i]:SetScript("OnUpdate", self.FlashButton)
		end

		-- touch up frames
		frame:SetScale(db.scale)
		self:StyleButtons(frameName)
		self:AlignButtons(frameName)
		self:ResizeFrame(frameName)
	end

	-- position frames after creating all of them, in case they're anchored to each other
	for i = 1, 3 do
		self:RepositionFrame(frames[i])
	end

	self.ImbueFrame.buttons[1]:SetID(16) -- main hand
	self.ImbueFrame.buttons[2]:SetID(17) -- off hand
end

function SimpleAuras:ResizeFrame(frameName)	-- resizes frame to fit its buttons
	local db = self.db.profile[frameName]
	local _, fontSize = self[frameName].buttons[1].duration:GetFont()
	self[frameName]:SetHeight(((db.iconSize + db.rowOffset) * ceil(#self[frameName].buttons / db.maxColumns)) - db.rowOffset + 4 + fontSize) -- (button+offset) * rowCount - offset + border + durationText
	self[frameName]:SetWidth((db.iconSize + db.columnOffset) * db.maxColumns - db.columnOffset + 4) -- (button+offset) * maxColumns - offset + border
end

function SimpleAuras:RepositionFrame(frameName)
	local db = self.db.profile[frameName]
	self[frameName]:ClearAllPoints()
	self[frameName]:SetPoint(db.point, db.relativeTo, db.relativePoint, db.xOffset, db.yOffset)
end

function SimpleAuras:StyleButtons(frameName)
	local db = self.db.profile[frameName]
	local durationFont = media and media:Fetch("font", db.durationFont) or _G["GameFontNormalSmall"]:GetFont()
	local stackFont = media and media:Fetch("font", db.stackFont) or _G["NumberFontNormal"]:GetFont()

	local button
	for i = 1, #self[frameName].buttons do
		button = self[frameName].buttons[i]

		button:SetHeight(db.iconSize)
		button:SetWidth(db.iconSize)

		button.duration:SetFont(durationFont, db.durationSize, db.durationFlag)
		button.count:SetFont(stackFont, db.stackSize, db.stackFlag)
	end
end

function SimpleAuras:RedrawFrames()
	self:SetTimeFormat()
	
	local frameName
	for i = 1, 3 do
		frameName = frames[i]
		self[frameName]:SetScale(self.db.profile[frameName].scale)
		self:RepositionFrame(frameName)
		self:ResizeFrame(frameName)
		self:AlignButtons(frameName)
		self:StyleButtons(frameName)
	end

	if not self.unlocked then -- trigger buff update if frames are locked
		self:PLAYER_AURAS_CHANGED()
		self:ScanImbues()
	else 
		local color
		for i = 1, 3 do
			frameName = frames[i]
			for i = 1, #self[frameName].buttons do
				self[frameName].buttons[i].duration:SetFormattedText(FormatTime(random(3600))) -- redraw dummy duration
				if frameName == "DebuffFrame" then -- redraw dummy debuff borders
					color = DebuffTypeColor[GetRandomArgument("none", "Magic", "Curse", "Disease", "Poison")]
					self.DebuffFrame.buttons[i].border:SetVertexColor(color.r, color.g, color.b)
				end
			end
		end
	end
end

function SimpleAuras:LockFrames()
	if self.unlocked then -- lock frames
		local frame, button
		for i = 1, 3 do
			frame = self[frames[i]]
			frame:EnableMouse(false)
			frame:SetBackdropColor(0, 0, 0, 0)
			for i = 1, #frame.buttons do -- reset buttons
				button = frame.buttons[i]
				button.count:SetText("")
				button:EnableMouse(true)
			end
		end

		self:PLAYER_AURAS_CHANGED()
		self:ScanImbues()
		self:OnEnable() -- reregister events

		self.unlocked = false
		self:Print(L["Frames locked"])
	else -- unlock frames
		local frameColors = { {0,1,0}, {1,0,0}, {0,0,1} }

		self:UnregisterAllEvents()
		self.BuffFrame:SetScript("OnUpdate", nil)

		local frame, button, color
		for i = 1, 3 do
			frame = self[frames[i]]
			frame:EnableMouse(true)
			frame:SetBackdropColor(frameColors[i][1], frameColors[i][2], frameColors[i][3], .3)

			for i = 1, #frame.buttons do -- show dummy icons
				button = frame.buttons[i]
				button.icon:SetTexture([[Interface\Icons\Temp]])
				if frame == self.DebuffFrame then
					color = DebuffTypeColor[GetRandomArgument("none", "Magic", "Curse", "Disease", "Poison")]
					--self:SetBorderColor(button, color.r, color.g, color.b)
					button.border:SetVertexColor(color.r, color.g, color.b)
				end
				button.duration:SetVertexColor(NORMAL_FONT_COLOR.r, NORMAL_FONT_COLOR.g, NORMAL_FONT_COLOR.b)
				button.duration:SetFormattedText(FormatTime(random(3600)))
				button.count:SetText(i)
				button.flashing = false
				button:SetAlpha(1)
				button:EnableMouse(false)
				button:Show()
			end
		end

		-- special imbue labels
		self.ImbueFrame.buttons[1].count:SetText("MH")
		self.ImbueFrame.buttons[2].count:SetText("OH")
		
		self.unlocked = true
		self:Print(L["Frames unlocked"])
	end
end
