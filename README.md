# SimpleAuras
Simple aura display addon for patch 2.4.3

## Features
* Highly customizable
* Very lightweight, lower CPU usage than any other buff display addon, including default frames

## Screenshots
![Auras](https://i.imgur.com/Wlj9zYL.png)

![Config](https://i.imgur.com/byJq2OI.png)