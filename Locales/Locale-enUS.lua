local L = LibStub("AceLocale-3.0"):NewLocale("SimpleAuras", "enUS", true)

-- Anchor points
L["Top right"] = true
L["Top left"] = true
L["Bottom right"] = true
L["Bottom left"] = true
L["Top"] = true
L["Bottom"] = true
L["Right"] = true
L["Left"] = true
L["Center"] = true

-- Font effects
L["None"] = true
L["Outline"] = true
L["Thick outline"] = true
L["Monochrome"] = true

-- Anchor frames
L["Screen"] = true
L["Minimap"] = true
L["Buffs"] = true
L["Debuffs"] = true
L["Imbues"] = true
L["Custom frame"] = true

-- Grow directions
L["Up"] = true
L["Down"] = true

-- Time format
L["Time format"] = true
L["Blizzlike"] = true
L["Detailed"] = true

-- Config
L["Rows and columns"] = true
	L["Column spacing"] = true
	L["Row spacing"] = true
	L["Column direction"] = true
	L["Row direction"] = true
	L["Columns"] = true
L["Fonts"] = true
	L["Duration font"] = true
	L["Stack font"] = true
	L["Duration font size"] = true
	L["Stack font size"] = true
	L["Duration font effect"] = true
	L["Stack font effect"] = true
L["Size and scale"] = true
	L["Icon size"] = true
	L["Scale"] = true
L["Position"] = true
	L["Horizontal offset"] = true
	L["Vertical offset"] = true
	L["Anchor point"] = true
	L["Relative point"] = true
	L["Anchor to"] = true
	L["Enter the global name of a frame to anchor to"] = true
		L["Frame not found"] = true
		L["Can't anchor a frame to itself"] = true
		L["Target frame is anchored to this frame"] = true

L["Unlock frames"] = true
L["Frames locked"] = true
L["Frames unlocked"] = true
