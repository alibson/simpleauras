local L = LibStub("AceLocale-3.0"):GetLocale("SimpleAuras")
local media = LibStub("LibSharedMedia-3.0", true)

SimpleAuras.defaults = {
	profile = {
		timeFormat = 2,

		['**'] = { -- inherited defaults
			iconSize = 30,
			durationFont = "Friz Quadrata TT",
			durationSize = 10,
			durationFlag = "none",
			stackFont = "Arial Narrow",
			stackSize = 14,
			stackFlag = "OUTLINE",

			point = "TOPRIGHT",
			relativePoint = "TOPRIGHT",
			rowOffset = 15,
			columnOffset = 5,
			columnDirection = "LEFT",
			rowDirection = "DOWN",
			scale = 1,

			custom = false,
			customAnchor = "UIParent",
		},
		BuffFrame = {
			maxButtons = 32,
			maxColumns = 16,

			relativeTo = "UIParent",
			xOffset = -220.5,
			yOffset = -10.5,
		},
		DebuffFrame = {
			maxButtons = 16,
			maxColumns = 8,

			relativeTo = "SAF_BuffFrame",
			relativePoint = "BOTTOMRIGHT",
			xOffset = 0,
			yOffset = -5,
			scale = 2,
		},
		ImbueFrame = {
			maxButtons = 2,
			maxColumns = 1,

			point = "TOPLEFT",
			relativeTo = "SAF_BuffFrame",
			xOffset = 5,
			yOffset = 0,
		},
	}
}

function SimpleAuras:GetOptions()
	local anchorPoints = {
		["TOPRIGHT"] = L["Top right"],
		["TOPLEFT"] = L["Top left"],
		["BOTTOMRIGHT"] = L["Bottom right"],
		["BOTTOMLEFT"] = L["Bottom left"],
		
		["TOP"] = L["Top"],
		["BOTTOM"] = L["Bottom"],
		["RIGHT"] = L["Right"],
		["LEFT"] = L["Left"],

		["CENTER"] = L["Center"],
	}
	local fontEffects = {
		["none"] = L["None"],
		["OUTLINE"] = L["Outline"],
		["THICKOUTLINE"] = L["Thick outline"],
		["MONOCHROME"] = L["Monochrome"],
	}

	local function hideMedia()
		return not media
	end
	local function GetFontList()
		local t = {}
		for k in pairs(media:HashTable("font")) do
			t[k] = k
		end
		return t
	end

	-- recursive function that goes up the anchor tree until UIParent to check if any are anchored to the frame
	local function CheckRelatives(sourceFrameName, targetFrame)
		if targetFrame:GetName() == sourceFrameName then
			return false
		else
			local n = targetFrame:GetNumPoints()
			for i = 1, n do
				local _, relativeTo = targetFrame:GetPoint(i)
				if relativeTo and not CheckRelatives(sourceFrameName, relativeTo) then
					return false
				end
			end
			return true -- all relatives of targetFrame checked
		end
	end

	local function CheckAnchor(info, val)
		local targetFrame = _G[val]
		if targetFrame and targetFrame.IsObjectType and targetFrame:IsObjectType("Region") then -- check if frame exists and can be anchored to
			local sourceFrame = "SAF_"..info[#info-1]
			if val ~= sourceFrame then -- check if trying to anchor a frame to itself
				-- check if targetFrame or its relatives are anchored to this frame
				return CheckRelatives(sourceFrame, targetFrame) and true or L["Target frame is anchored to this frame"]
			else
				return L["Can't anchor a frame to itself"]
			end
		else
			return L["Frame not found"]
		end
	end

	local function GetOptionsTemplate(name, displayName, order)
		return {
			name = displayName,
			type = 'group',
			order = order,
			args = {
				header_alignment = {
					type = 'header',
					name = L["Rows and columns"],
					order = 1,
				},
				columnOffset = {
					type = 'range',
					name = L["Column spacing"],
					min = 0,
					max = 50,
					step = 1,
					order = 2,
				},
				rowOffset = {
					type = 'range',
					name = L["Row spacing"],
					min = 0,
					max = 50,
					step = 1,
					order = 3,
				},
				columnDirection = {
					type = 'select',
					name = L["Column direction"],
					values = {
						["LEFT"] = L["Left"],
						["RIGHT"] = L["Right"],
					},
					order = 4,
				},
				rowDirection = {
					type = 'select',
					name = L["Row direction"],
					values = {
						["UP"] = L["Up"],
						["DOWN"] = L["Down"],
					},
					order = 5,
				},
				maxColumns = {
					type = 'range',
					name = L["Columns"],
					min = 1,
					max = self.defaults.profile[name].maxButtons,
					step = 1,
					validate = function(info, val)
						local frameName = info[#info-1]
						if val < 1 or val > self.db.profile[info[#info-1]].maxButtons then
							return false
						else
							return true
						end
					end,
					order = 6,
				},
				------------------------------
				header_fonts = {
					type = 'header',
					name = L["Fonts"],
					order = 10,
				},
				durationFont = {
					type = 'select',
					name = L["Duration font"],
					values = GetFontList,
					hidden = hideMedia,
					order = 11,
				},
				stackFont = {
					type = 'select',
					name = L["Stack font"],
					values = GetFontList,
					hidden = hideMedia,
					order = 12,
				},
				durationSize = {
					type = 'range',
					name = L["Duration font size"],
					min = 1,
					max = 50,
					step = 1,
					order = 13,
				},
				stackSize = {
					type = 'range',
					name = L["Stack font size"],
					min = 1,
					max = 50,
					step = 1,
					order = 14,
				},
				durationFlag = {
					type = 'select',
					name = L["Duration font effect"],
					values = fontEffects,
					order = 15,
				},
				stackFlag = {
					type = 'select',
					name = L["Stack font effect"],
					values = fontEffects,
					order = 16,
				},
				------------------------------
				header_size = {
					type = 'header',
					name = L["Size and scale"],
					order = 20,
				},
				iconSize = {
					type = 'range',
					name = L["Icon size"],
					min = 1,
					max = 100,
					step = 1,
					order = 21,
				},
				scale = {
					type = 'range',
					name = L["Scale"],
					min = 0.1,
					max = 10,
					step = 0.1,
					order = 22,
				},
				------------------------------
				header_position = {
					type = 'header',
					name = L["Position"],
					order = 30,
				},
				xOffset = {
					type = 'range',
					name = L["Horizontal offset"],
					min = -500,
					max = 500,
					step = 0.1,
					order = 31,
				},
				yOffset = {
					type = 'range',
					name = L["Vertical offset"],
					min = -500,
					max = 500,
					step = 0.1,
					order = 32,
				},
				point = {
					type = 'select',
					name = L["Anchor point"],
					values = anchorPoints,
					order = 33,
				},
				relativePoint = {
					type = 'select',
					name = L["Relative point"],
					values = anchorPoints,
					order = 34,
				},
				relativeTo = {
					type = 'select',
					name = L["Anchor to"],
					get = function(info)
						if self.db.profile[info[#info-1]].custom then
							return "Custom"
						else
							return self.db.profile[info[#info-1]].relativeTo
						end
					end,
					set = function(info, val)
						local frameName = info[#info-1]
						if val == "Custom" then
							self.db.profile[frameName].custom = true
							self.db.profile[frameName].relativeTo = self.db.profile[frameName].customAnchor
						else
							self.db.profile[frameName].custom = false
							self.db.profile[frameName].relativeTo = val
						end
						self:RepositionFrame(frameName)
					end,
					values = function(info)
						local anchorFrames = {
							["UIParent"] = L["Screen"],
							["MinimapCluster"] = L["Minimap"],
							["SAF_BuffFrame"] = L["Buffs"],
							["SAF_DebuffFrame"] = L["Debuffs"],
							["SAF_ImbueFrame"] = L["Imbues"],
							["Custom"] = L["Custom frame"],
						}
						anchorFrames["SAF_"..info[#info-1]] = nil -- remove self from selection list so you don't try to anchor the frame to itself
						return anchorFrames
					end,
					validate = function(info, val)
						return val == "Custom" and true or CheckAnchor(info, val)
					end,
					order = 35,
				},
				customFrame = {
					type = 'input',
					name = L["Custom frame"],
					desc = L["Enter the global name of a frame to anchor to"],
					get = function(info)
						return self.db.profile[info[#info-1]].customAnchor
					end,
					set = function(info, val)
						local frameName = info[#info-1]
						self.db.profile[frameName].relativeTo = val
						self.db.profile[frameName].customAnchor = val
						self:RepositionFrame(frameName)
					end,
					validate = CheckAnchor,
					disabled = function(info)
						return not self.db.profile[info[#info-1]].custom
					end,
					order = 36,
				},
			},
		}
	end

	local options = {
		name = "SimpleAuras",
		handler = self,
		type = 'group',
		childGroups = 'tree',
		get = function(info)
			if info[#info-1] == "SimpleAuras" then -- root level
				return self.db.profile[info[#info]]
			else
				return self.db.profile[info[#info-1]][info[#info]]
			end
		end,
		set = function(info, val)
			if not info[#info-1] then -- root level
				self.db.profile[info[#info]] = val
			else
				self.db.profile[info[#info-1]][info[#info]] = val
			end
			self:RedrawFrames()
		end,
		args = {
			BuffFrame = GetOptionsTemplate("BuffFrame", L["Buffs"], 10),
			DebuffFrame = GetOptionsTemplate("DebuffFrame", L["Debuffs"], 20),
			ImbueFrame = GetOptionsTemplate("ImbueFrame", L["Imbues"], 30),
			lock = {
				type = 'toggle',
				name = L["Unlock frames"],
				get = function() return self.unlocked end,
				set = "LockFrames",
				order = 1,
			},
			timeFormat = {
				type = 'select',
				name = L["Time format"],
				values = {
					[1] = L["Blizzlike"],
					[2] = L["Detailed"],
				},
				order = 2,
			},
		},
	}
	options.args.profile = LibStub("AceDBOptions-3.0"):GetOptionsTable(self.db) 
	return options
end
