## Interface: 20400
## Title: SimpleAuras
## Version: 1.1
## Author: Alibson 
## Notes: Just a simple buff display
## SavedVariables: SimpleAurasDB
## OptionalDeps: Ace3, LibSharedMedia-3.0, ButtonFacade
## X-Embeds: Ace3
## X-Category: Buffs

embeds.xml

Locales\Locales.xml
Core.lua
Templates.xml
Options.lua
